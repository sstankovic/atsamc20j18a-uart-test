# ATSAMC20J18A-uart-test 

This project consists of firmware and python module intend to test USART of [ATSAMC20J18A](https://www.microchip.com/wwwproducts/en/ATSAMC20J18A).

# Firmware
Firmware is located in `UART-Bridge` folder. It is writen in AtmelStudio using AtmelStart.

Firmware logic is to collects bytes from Rx and waiting for delimiter ';'. When delimiter is received it Tx all received bytes. 

**NOTE,** that our board is custom and has RS-485 driver connected to ATSAMC20J18A USART. So you can find handling of direction pins in firmware.

### Dependencies
 * AtmelStudio
 * AtmelStart
 * Custom board with ATSAMC20J18A on 20MHz external clock on 5V DC

# Python module
Python module is located in `test-uart` folder. It is test module which expect to receive what is send. String which is sending and number of polls is configurable in `test_usart.py` module.

### Dependencies
 * Python3
 * install requirements by invoking `pip3 install -r requirements` from `test-uart` folder

 At the end of test terminal will print number of SUCCESS and FAILED polls.