#include <atmel_start.h>

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	gpio_set_pin_level(rs485_de, 0);
	gpio_set_pin_level(rs485_re, 0);
	/* Replace with your application code */
	while (1) {
		procces_messages();
	}
}
