/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>

/*! The buffer size for USART */
#define USART_0_BUFFER_SIZE			16
#define MAXIMUM_RECEIVE_PACKET_SIZE 256
#define DELIMITER					0x3B //0x3B is hex presentation of character ';'

struct usart_async_descriptor USART_0;

static uint8_t received_bytes[MAXIMUM_RECEIVE_PACKET_SIZE];
static uint8_t USART_0_buffer[USART_0_BUFFER_SIZE];

static volatile bool is_byte_sent = 0;
static bool message_received = false;
/**
 * \brief USART Clock initialization function
 *
 * Enables register interface and peripheral clock
 */
void USART_0_CLOCK_init()
{

	hri_gclk_write_PCHCTRL_reg(GCLK, SERCOM2_GCLK_ID_CORE, CONF_GCLK_SERCOM2_CORE_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_gclk_write_PCHCTRL_reg(GCLK, SERCOM2_GCLK_ID_SLOW, CONF_GCLK_SERCOM2_SLOW_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_mclk_set_APBCMASK_SERCOM2_bit(MCLK);
}

/**
 * \brief USART pinmux initialization function
 *
 * Set each required pin to USART functionality
 */
void USART_0_PORT_init()
{

	gpio_set_pin_function(PA08, PINMUX_PA08D_SERCOM2_PAD0);

	gpio_set_pin_function(PA09, PINMUX_PA09D_SERCOM2_PAD1);
}

static void tx_cb_USART_0(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	is_byte_sent = 1;
}

static void rx_cb_USART_0(const struct usart_async_descriptor *const io_descr)
{
	message_received = true;
}

uint8_t usart_receive_handler(void){
	uint8_t read_byte = 0;
	struct io_descriptor *io;
	usart_async_get_io_descriptor(&USART_0, &io);
	
	io_read(io, &read_byte, 1);

	return read_byte;
}

/**
 * \brief USART initialization function
 *
 * Enables USART peripheral, clocks and initializes USART driver
 */
void USART_0_init(void)
{
	USART_0_CLOCK_init();
	usart_async_init(&USART_0, SERCOM2, USART_0_buffer, USART_0_BUFFER_SIZE, (void *)NULL);
	usart_async_register_callback(&USART_0, USART_ASYNC_RXC_CB, rx_cb_USART_0);
	usart_async_register_callback(&USART_0, USART_ASYNC_TXC_CB, tx_cb_USART_0);
	usart_async_enable(&USART_0);
	USART_0_PORT_init();
}

void system_init(void)
{
	init_mcu();

	// GPIO on PA06

	gpio_set_pin_level(rs485_re,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(rs485_re, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(rs485_re, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PA07

	gpio_set_pin_level(rs485_de,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(rs485_de, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(rs485_de, GPIO_PIN_FUNCTION_OFF);

	USART_0_init();
}

static uint8_t current_message_received_bytes = 0;

void procces_messages(void){
	
	if(message_received)
	{
		received_bytes[current_message_received_bytes] = usart_receive_handler();
		message_received = false;
		current_message_received_bytes++;

		if(received_bytes[current_message_received_bytes-1] == DELIMITER)
		{
			gpio_set_pin_level(rs485_de, 1);
			gpio_set_pin_level(rs485_re, 1);

			struct io_descriptor *io;
			usart_async_get_io_descriptor(&USART_0, &io);

			for(int i = 0; i < current_message_received_bytes; i++) {
				is_byte_sent = 0;
				io_write(io, &received_bytes[i], 1);
				while(!is_byte_sent);
			}

			current_message_received_bytes = 0;

			gpio_set_pin_level(rs485_de, 0);
			gpio_set_pin_level(rs485_re, 0);
		}
	}

}