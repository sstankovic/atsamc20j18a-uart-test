#!/usr/bin/python3
"""
Project	 	: USART/UART test script 
Date&Time	: 06th August 2019.
Description	: 
"""
import serial
import logging
import time, sys


class UsartTest:
    def __init__(
        self,
        portName="",
        baudRate=115200,
        bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        timeout=0.05,
    ):
        self.portName = portName
        self.baudRate = baudRate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout

    def openComPort(self):
        try:
            self.ser = serial.Serial(
                self.portName,
                self.baudRate,
                timeout=self.timeout,
                bytesize=self.bytesize,
                parity=self.parity,
                stopbits=self.stopbits,
            )
            time.sleep(0.5)
            return True
        except Exception as e:
            logging.error(
                "Couldn't open desired tty/COMx port: "
                + self.portName
                + "\nError: "
                + str(e)
            )
            sys.exit()

    def closeComPort(self):
        try:
            self.ser.close()
        except Exception as e:
            logging.error("Couldn't close tty/COMx port" + "\nError: " + str(e))
            sys.exit()

    def sendData(self, data):
        self.data = data

        try:
            self.ser.write(self.data)
            print("\n")
            logging.debug("Sent are: " + str(self.data))

            received = self.ser.read(sys.getsizeof(self.data))
            logging.debug("Received are: " + str(received) + "\n\r")

            if received != data:
                return False

            return True
        except Exception as e:
            logging.error("Couldn't write on " + self.portName + "\nERROR: " + str(e))
            return False
# EOF