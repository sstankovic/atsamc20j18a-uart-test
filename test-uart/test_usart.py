#!/usr/bin/python3
"""
Project	 	: USART/UART test script 
Date&Time	: 06th August 2019.
Description	: 
"""
import time, sys
import logging
from datetime import datetime
from test_usart_api import UsartTest

COMPORT_NAME 	= "COM11"
VERSION 		= "0.0.3"
NUMBER_OF_POLLS = 200

MESSAGE_TO_SEND = "This file will be overwritten when reconfiguring your Atmel Start project.;"

logging.basicConfig(level=logging.DEBUG)


def main():
    usart_test = UsartTest(portName=COMPORT_NAME)
    usart_test.openComPort()

    while 1:
        print(
            "\n\r-----------------\n\rChoose operation:\n\r1 - Send Data\n\rq - Quit program\n\r-----------------"
        )
        try:
            in_selection = ""
            in_selection = input(">> ")
        except Exception as e:
            logging.error("Error: " + str(e))
            break
        else:
            if in_selection == "1":
                number_of_fails = 0

                for x in range(NUMBER_OF_POLLS):
                    print("\r" + "Number of polls is: " + str(x + 1), end="")
                    if not usart_test.sendData(MESSAGE_TO_SEND.encode("utf-8")):
                        logging.error(
                            "Failed to send data for x = "
                            + str(x + 1)
                            + " on "
                            + str(datetime.now()) + "\n\r"
                        )
                        number_of_fails += 1

                    time.sleep(0.05)

                print(
                    "\n\r********************************\n\r"
                    + "Number of FAILED polls is: "
                    + str(number_of_fails)
                )
                print(
                    "\r"
                    + "Number of SUCCESS polls is: "
                    + str(x + 1 - number_of_fails)
                    + "\n\r********************************\n\r"
                )

            elif in_selection == "q":
                usart_test.closeComPort()
                print("Thank you for using USART/UART test script.")
                time.sleep(0.3)
                sys.exit()
            else:
                print("Wrong selection.\n")


if __name__ == "__main__":
    main()
    print("End of module")
# EOF
